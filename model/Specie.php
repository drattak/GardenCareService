<?php

use Illuminate\Database\Eloquent\Model;

class Specie extends Model {

    protected $table  = 'specie';

    protected $primaryKey = 'idSpecie';

    protected $fillable = ['idSpecie','name','growth_time','wet_hour','wet_advice','image'];

    public $timestamps = false;

}