<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$config['displayErrorDetails'] = true; //in Dev mode only
$config['addContentLengthHeader'] = false;

session_start();

// Database config
$config['db']['host']   = 'localhost';
$config['db']['user']   = 'gardencare';
$config['db']['pass']   = 'gardencare';
$config['db']['dbname'] = 'gardencare';

/**
 * Needed to determine route name into middleware
 * Needed for TwigExtension_currentRouteIs
 */
$config['determineRouteBeforeAppMiddleware'] = true;

spl_autoload_register(
    function ($classname) {
        include '../model/' . $classname . '.php';
    }
);

$app = new \Slim\App(['settings' => $config]);

/*Setting : DB, View and log*/
require_once '../src/settings.php';
require_once '../src/routes.php';


$app->run();