<?php

use Illuminate\Database\Eloquent\Model;

class Person extends Model {

    protected $table  = 'person';

    protected $primaryKey = 'idPerson';

    protected $fillable = ['idPerson','firstname','lastname','email','password'];

    public $timestamps = false;

    public function getGardens() {
        return Garden::where('person', '=', $this->idPerson)->get();
    }

    public function getPlants() {

        $gardens = $this->getGardens();
        $plants = array();

        foreach ($gardens as $garden) {
            $gardenPlants = $garden->getPlants();
            foreach ($gardenPlants as $gardenPlant) {
                $plants[] = $gardenPlant;
            }
        }

        return $plants;
    }

}

