<?php

use Illuminate\Database\Eloquent\Model;

class Garden extends Model {

    protected $table  = 'garden';

    protected $primaryKey = 'idGarden';

    protected $fillable = ['idGarden','person','name','city'];

    public $timestamps = false;


    public function getPlants() {
        return Plant::where('garden', '=', $this->idGarden)->get();
    }

    public function getPerson() {
        return Person::find($this->person)->get()->first();
    }

}