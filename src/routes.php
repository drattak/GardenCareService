<?php
// Routes

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


//Specie

$app->get('/specie/{id}', function(Request $request,Response $response, $args) {

    $specie = Specie::find($args['id']);

    return $response->withJson($specie);
});

$app->get('/specie', function(Request $request,Response $response, $args) {

    $specie = Specie::where('name','=',$request->getQueryParams("name"))->get()->first();

    return $response->withJson($specie);
});

$app->get('/species', function(Request $request,Response $response, $args) {

    $species = Specie::all();

    return $response->withJson($species);
});

//Plant

$app->get('/plant/{id}', function(Request $request,Response $response, $args) {

    $plant = Plant::find($args['id']);
    return $response->withJson($plant);
});

$app->put('/plant/{id}', function(Request $request,Response $response, $args) {

    $plant = Plant::find($args['id']);

    $parsedBody = $request->getParsedBody();

    $mature = $parsedBody['mature'];

    if($mature)
        $plant->mature = $mature;

    $plant->save();

    return $response->withJson($plant);
});


$app->get('/plants', function(Request $request,Response $response, $args) {

    $plants = Plant::all();

    return $response->withJson($plants);
});

$app->post('/plant/add',function(Request $request,Response $response, $args) {

    $parsedBody = $request->getParsedBody();

    $plant = Plant::create([
        "mature" => 'no',
        "specie" => $parsedBody['specie'],
        "garden" => $parsedBody['garden'],
        "plantedAt" => $parsedBody['plantedAt']
    ]);

    return $response->withJson($plant);
});

//Garden

$app->get('/garden/{id}', function(Request $request,Response $response, $args) {

    $garden = Garden::find($args['id']);

    return $response->withJson($garden);
});


$app->get('/garden/{id}/plants', function(Request $request,Response $response, $args) {

    $garden = Garden::find($args['id']);

    $plants = $garden->getPlants();

    return $response->withJson($plants);
});

$app->get('/gardens', function(Request $request,Response $response, $args) {

    $gardens = Garden::all();

    return $response->withJson($gardens);
});

$app->post('/garden/add',function(Request $request,Response $response, $args) {

    $parsedBody = $request->getParsedBody();

    $garden = Garden::create([
        "person" => $parsedBody['person'],
        "name" => $parsedBody['name'],
        "city" => $parsedBody['city']
    ]);

    return $response->withJson($garden);
});


//Person



$app->get('/person', function(Request $request,Response $response, $args) {

    $person = Person::where('email','=',$request->getQueryParams("email"))->get()->first();


    return $response->withJson($person);
});


$app->get('/person/{email}/gardens', function(Request $request,Response $response, $args) {

    $person = Person::where('email','=',$args['email'])->get()->first();

    $gardens = $person->getGardens();

    return $response->withJson($gardens);
});

$app->get('/person/{email}/plants', function(Request $request,Response $response, $args) {

    $person = Person::where('email','=',$args['email'])->get()->first();

    $plants = $person->getPlants();

    return $response->withJson($plants);
});

$app->post('/login', function (Request $request, Response $response) {

    $parsedBody = $request->getParsedBody();

    $email = $parsedBody['email'];
    $passwordGiven = $parsedBody['password'];

    $person = Person::where('email', '=', $email)->get()->first();

    $resBoolean = 'false';

    if(password_verify($passwordGiven,$person->password))
        $resBoolean = 'true';

    return $response->write($resBoolean);
});
