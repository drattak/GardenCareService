<?php

use Illuminate\Database\Eloquent\Model;

class Plant extends Model {

    protected $table  = 'plant';

    protected $primaryKey = 'idPlant';

    protected $fillable = ['idPlant','mature','specie','plantedAt','garden'];

    public $timestamps = false;

    public function getPerson() {

        $garden = Garden::find($this->garden)->get()->first();

        return $garden->getPerson();
    }

}