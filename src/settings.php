<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * Container properties
 */
$container = $app->getContainer();
/**
 * Set logger
 * use : $this->logger->addInfo('Access register: GET');
 */
$container['logger'] = function ($c) {
    $logger = new \Monolog\Logger('fuelmycar_logger');
    $file_handler = new \Monolog\Handler\StreamHandler('../logs/app.log');
    $logger->pushHandler($file_handler);
    return $logger;
};


$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection(
    [
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'gardencare',
        'username' => 'gardencare',
        'password' => 'gardencare',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
    ]
);
$capsule->setAsGlobal();
$capsule->bootEloquent();


/**
 * Set DB
 */
$container['db'] = function ($c) use ($capsule) {
    return $capsule;
};

